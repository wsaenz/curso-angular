export class DestinoViaje {
  private selected: boolean;
  public servicios: string[];
  constructor(public n: string, public u: string) {
    this.servicios=[
      'Piscina',
      'Desayuno'
    ]
   }
  isSelcted(): boolean {
    return this.selected;
  }
  setSelected(s: boolean) {
    this.selected = s;
  }
}
