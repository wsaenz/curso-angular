import { DestinoViaje } from './../models/destino-viaje.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss']
})
export class ListaDestinosComponent implements OnInit {

  destinos: DestinoViaje[];

  constructor() {
    this.destinos=[]
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean{
    this.destinos.push(new DestinoViaje(nombre, url));
    return false;
  }

  elegido(destino: DestinoViaje){
    this.destinos.forEach(function (x) {x.setSelected(false); });
    destino.setSelected(true);
  }
}
